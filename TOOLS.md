<!--
SPDX-FileCopyrightText: 2023 Melvin Keskin <melvo@olomono.de>

SPDX-License-Identifier: AGPL-3.0-or-later
-->

# Tools

[[_TOC_]]

## Base Tool

Provider maintenance tools are available using `tools/run.py`. To get a list of possible commands, run:

```shell
python3 -m tools.run -h
```

Help for individual commands is available by placing `-h` behind the command. For example, when running the `check_urls` tool with:

```shell
python3 -m tools.run check_urls -h
```

For debugging, run the base tool with:

```shell
python3 -m tools.run -d <command>
```

## Property Manager

The tool `tools/property_manager.py` can be used to manage the properties providers have.
It modifies the property definitions in `properties.json`.

Additonally, it adds new properties to the providers file or removes existing properties from it.
However, it does neither change the criteria for filtering providers in `tools/criteria.py` nor their documentation in `README.md`.
Thus, you need to do that manually if needed.

### Usage

An interactive dialog opens when you run the tool:

```shell
python3 -m tools.run property_manager
```

## Filter Tool

The tool `tools/filter.py` can be used to create filtered lists of providers (called **provider lists**).

### Usage

Provider lists for all categories are created if you run the tool without arguments:

```shell
python3 -m tools.run filter
```

If you want to create a filtered list which can be used by a client, simply enter the category name.

Example for creating a list of category **A** providers:

```shell
python3 -m tools.run filter -A
```

You can create a provider list containing all providers for completely customized filtering (e.g., by an own filter tool or at runtime):

```shell
python3 -m tools.run filter -C
```

A **s**imple list containing only the domains of the providers is also possible.

Example for creating a domain list of category **D** providers to use it only for autocomplete:

```shell
python3 -m tools.run filter -s -C
```

The **c**ategories of the filtered providers can be included in their entries.

Example for creating a list of category **D** providers that includes their best categories.

```shell
python3 -m tools.run filter -c -D
```

You can create files containing the **r**esults of the filtering (called **categorization results**).
Those files include the providers' properties that do not meet the criteria for being in specific categories.

Example for creating result files for all providers:

```shell
python3 -m tools.run filter -r
```

If you are interested in specific providers, you can append them to the command.

Example for creating a list of category **B** providers out of *example.org* and *example.com*:

```shell
python3 -m tools.run filter -B example.org example.com
```

The tool can be run in **d**ebug mode to see why providers are not in a specific category.

Example for creating a list of category **A** providers and logging additional information:

```shell
python3 -m tools.run -d filter -A
```

Furthermore, the arguments can be combined to show which criteria specific providers do not meet for being in a specific category.

Example for creating a list of category **A** providers out of *example.org* and *example.com* and logging additional information:

```shell
python3 -m tools.run -d filter -A example.org example.com
```

## Badge Tool

The tool `tools/badge.py` can be used to create badges for the specified categories.
It uses the files created by the filter tool.
Thus, the filter tool must be run before running the badge tool.

Template files are used for generating the badges.
All badges contain the category.
Additionally, it is possible to create badges containing the count of providers in a specific category.

The badge tool has two phases:

1. It generates badges for all categories.
1. It creates the directory `badges` and fills it with badges for all providers (called **provider badges**).

### Usage

Badges for all categories are created if you run the tool without arguments:

```shell
python3 -m tools.run badge
```

The badges for providers can be created as symbolic **l**inks instead of regular files:

```shell
python3 -m tools.run badge -l
```

If you want to create a badge which can be used by a provider, simply enter the category name.

Example for creating a badge for category **A**:

```shell
python3 -m tools.run badge -A
```

Badges containing the **c**ount of providers in specific categories are also possible.

Example for creating a badge containing the count of providers in category **C** in addition to the normal badges:

```shell
python3 -m tools.run badge -c -C
```

## URL Checker

The tool `tools/check_urls.py` can be used to check URLs.
It succeeds if all checked URLs are reachable.

### Usage

The URLs of all appropriate files are checked if you run the tool without arguments:

```shell
python3 -m tools.run check_urls
```

It is also possible to check only the URLs in specific files.

Example for checking the URLs in `providers.json` and `tools/filter.py`:

```shell
python3 -m tools.run check_urls providers.json tools/filter.py
```

## Automation

The providers file is automatically kept up up-do-date via **automation tools**.
They retrieve data from external sources and write changes to the providers file.

### GitLab

The tool `automation/gitlab.py` can be used to automatically gather updates for the providers file and create merge requests for them.

#### Usage

A [project API token](https://invent.kde.org/melvo/xmpp-providers/-/settings/access_tokens) for creating merge requests in the main repository is needed to run the tool.
Additionally, for creating branches and commits in the [GitLab bot's repository](https://invent.kde.org/xmppprovidersbot/xmpp-providers), the tool needs the bot's [personal API token](https://invent.kde.org/-/profile/personal_access_tokens) and its email address:

```shell
python3 -m tools.run gitlab_bot <main project token> <bot personal token> <bot email address>
```

Example:

```shell
python3 -m tools.run gitlab_bot glght-kasjlkadg8ADSFGGasfg glghh-jasjlkadg8ADSFGGasfh alice@example.org
```

### Web

The tool `automation/web.py` can be used to query provider ratings of external services.

#### Usage

The ratings are queried and the corresponding properties in the providers file changed (when necessary) if you run the tool without arguments:

```shell
python3 -m tools.run web_bot
```

You can also trigger **u**pdates of ratings supporting that (querying and updating a rating cannot be done at the same time):

```shell
python3 -m tools.run web_bot -u
```
