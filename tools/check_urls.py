#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2022 Melvin Keskin <melvo@olomono.de>
# SPDX-FileCopyrightText: 2023 Daniel Brötzmann <daniel.broetzmann@posteo.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Checks whether URLs in files are reachable."""

import json
import logging
import os
import re
import sys
from collections import defaultdict
from http import HTTPStatus
from multiprocessing.pool import ThreadPool
from urllib.parse import unquote

import requests
from requests.adapters import HTTPAdapter
from requests.adapters import Retry

log = logging.getLogger()

# Files/Directories with those paths are not checked.
EXCLUDED_PATHS = [
    ".git",
    ".gitlab",
    ".ruff_cache",
    ".vscode",
    "badges",
    "logos",
    "results",
    "tools/automation/gitlab.py",
    "tools/check_urls.py",
]

# Files with those parts in their paths are not checked (relevant for subpaths).
EXCLUDED_PATH_PARTS = [
    "__init__.py",
    "__pycache__",
]

# URLs containing those parts are not checked.
EXCLUDED_URL_PARTS = [
    "example.org",
    "<version>",
    "https://admin.thegreenwebfoundation.org/greencheck/",
    "https://compliance.conversations.im/server/",
    "https://compliance.conversations.im/live/",
    "https://invent.kde.org/melvo/xmpp-providers/-/settings/access_tokens",
]

# Redirects are allowed for URLs with those prefixes.
REDIRECTED_URL_PREFIXES = [
    "https://invent.kde.org/-/profile/personal_access_tokens",
    "https://invent.kde.org/melvo/xmpp-providers.git",
    "https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/master/",
    "https://jabber.at/api/set-lang/?lang=",
]

URL_REGEX = (
    r'https://[^\s"]*?\)|https://[^\s\)]*?"|https://[^"\;)]*?\s|https://[^\s"\)]*?;'
)

REQUEST_TIMEOUT_SECONDS = 10
MAX_REQUEST_ATTEMPTS = 5
REQUEST_RETRY_BACKOFF_FACTOR = 1


class CheckUrls:
    """Checks whether URLs for a given list of files or all files
    (if no files are specified) are reachable.

    Parameters
    ----------
    file_paths : list[str]
        list of relative file paths to be checked
    """

    def __init__(self, file_paths: list[str]) -> None:
        included_file_paths: list[str] = []
        excluded_file_paths: list[str] = []

        if file_paths:
            for file_path in file_paths:
                if file_path.startswith(tuple(EXCLUDED_PATHS)):
                    excluded_file_paths.append(file_path)
                else:
                    included_file_paths.append(file_path)
        else:
            log.debug("No files specified. Checking all files…")
            included_file_paths, excluded_file_paths = self._get_relevant_paths()

        included_file_paths = sorted(included_file_paths)
        excluded_file_paths = sorted(excluded_file_paths)

        log.info("Starting URL check for: %s", included_file_paths)
        log.debug("Excluded files/directories: %s", excluded_file_paths)

        seconds_between_failed_requests = []

        for i in range(MAX_REQUEST_ATTEMPTS):
            seconds_between_failed_requests.append(
                REQUEST_RETRY_BACKOFF_FACTOR * (2**i)
            )

        log.debug(
            "Seconds to wait between failed requests: %s",
            seconds_between_failed_requests,
        )

        unreachable_url_found = False
        for file_path in included_file_paths:
            if not os.path.exists(file_path):
                log.warning("File path does not exist: %s", file_path)
                continue
            urls = self._get_urls_from_file(file_path)
            urls_reachable = ThreadPool(16).imap_unordered(
                self._check_url, list(urls.items())
            )
            for url_reachable in urls_reachable:
                if not url_reachable:
                    unreachable_url_found = True

        if unreachable_url_found:
            sys.exit(1)

    def _get_urls_from_file(self, file_path: str) -> dict[str, list[str]]:
        """Extracts URLs from a file.
        Example: {"https://example.org": ["2", "5"]}

        Parameters
        ----------
        file_path : str
            relative path of file

        Returns
        -------
        dict[str, list[str]]
            extracted URLs and the lines whom contain them
        """

        urls = defaultdict(lambda: defaultdict(list))

        try:
            with open(file_path, "r") as check_file:
                line_number = 0

                for line in check_file:
                    line_number += 1
                    url_matches = re.findall(URL_REGEX, line)

                    for url_match in url_matches:
                        url = url_match[:-1]

                        if self._is_url_excluded(url, file_path, line_number):
                            continue

                        log.debug(
                            "Inserting for check %s:%s:\t%s",
                            file_path,
                            line_number,
                            url,
                        )

                        urls[url][file_path].append(line_number)

        except FileNotFoundError as error:
            log.error(error)

        return urls

    @staticmethod
    def _get_relevant_paths() -> tuple[list[str], list[str]]:
        """Gets all included and excluded paths from the current working directory,
        including paths of files in child directories.
        Only those files are included that are neither in EXCLUDED_PATHS nor in any
        child directory of a directory in EXCLUDED_PATHS.
        If a directory is excluded, its child directories are excluded as well.
        For directories, only the top-most directory of any excluded directory is
        returned.

        Returns
        -------
        tuple[list[str], list[str]]
            relative paths of included files and excluded files/directories
        """

        included_paths: list[str] = []
        excluded_paths: list[str] = []

        for directory_path, _dirs, file_names in os.walk("."):
            # Create a path without the prefix "./".
            simple_directory_path = directory_path[2:]

            # Add excluded parent directories.
            if simple_directory_path in EXCLUDED_PATHS + EXCLUDED_PATH_PARTS:
                excluded_paths.append(simple_directory_path)
                continue

            # Skip excluded child directories of the added parent directories.
            if simple_directory_path.startswith(
                tuple(f"{excluded_path}/" for excluded_path in EXCLUDED_PATHS)
            ):
                continue

            # Add excluded directories with specific path parts.
            if any(
                excluded_path_part in simple_directory_path
                for excluded_path_part in EXCLUDED_PATH_PARTS
            ):
                excluded_paths.append(simple_directory_path)
                continue

            # Process the files of included directories.
            for file_name in file_names:
                # Create a full path for files in (child) directories of the current
                # working directory.
                # Otherwise, use only the filename.
                file_path = (
                    f"{simple_directory_path}/{file_name}"
                    if simple_directory_path
                    else file_name
                )

                # Add excluded files.
                if file_path in EXCLUDED_PATHS or any(
                    excluded_path_part in file_path
                    for excluded_path_part in EXCLUDED_PATH_PARTS
                ):
                    excluded_paths.append(file_path)
                    continue

                # Add included files.
                included_paths.append(file_path)

        return included_paths, excluded_paths

    @staticmethod
    def _is_url_excluded(url: str, file_path: str, line_number: int) -> bool:
        """Checks whether a URL is excluded from the check.
        URLs are excluded when they contain EXCLUDED_URL_PARTS.

        Parameters
        ----------
        url : str
            URL to check
        file_path : str
            path of the file the URL is taken from
        line_number : int
            number of the line containing URL

        Returns
        -------
        bool
            whether url is excluded
        """

        for excluded_url_part in EXCLUDED_URL_PARTS:
            if url.find(excluded_url_part) != -1:
                log.debug(
                    "Excluding from check %s:%s:\t%s because it contains '%s'",
                    file_path,
                    line_number,
                    url,
                    excluded_url_part,
                )

                return True

        return False

    @staticmethod
    def _check_url(url_data: tuple[str, list[str]]) -> bool:
        """Starts a request to check whether a URL is reachable.

        Parameters
        ----------
        url_data : tuple[str, list[str]
            URL to check and its occurrences

        Returns
        -------
        bool
            whether url is reachable
        """

        url, occurrences = url_data

        log.debug("Checking %s in files/lines: %s", url, json.dumps(occurrences))

        # Allow redirects only for specific URLs.
        redirects_allowed = any(
            url.startswith(prefix) for prefix in REDIRECTED_URL_PREFIXES
        )

        try:
            # Retry a failed request until it succeeds but at most "total" times.
            # Increase the time before starting a next request attempt exponentially
            # for each retry and multiply it by "backoff_factor".
            # Seconds to wait between
            # request n and request n+1 = backoff_factor*(2**(n-1))
            adapter = HTTPAdapter(
                max_retries=Retry(
                    total=MAX_REQUEST_ATTEMPTS,
                    backoff_factor=REQUEST_RETRY_BACKOFF_FACTOR,
                )
            )

            session = requests.Session()
            session.mount("https://", adapter)

            # Percent-encoded URLs are decoded by "unquote()" because the GET
            # request expects them decoded.
            response = session.get(
                unquote(url),
                timeout=REQUEST_TIMEOUT_SECONDS,
                allow_redirects=redirects_allowed,
            )

            status_code = response.status_code
            log.debug("Received status code: %s", status_code)

            if status_code != HTTPStatus.OK:
                log.error(
                    "%s: INVALID status code %s in files / lines: %s",
                    url,
                    status_code,
                    json.dumps(occurrences),
                )

                return False

        except requests.RequestException as error:
            log.error(
                "Request failed for %s in files / lines: %s: %s",
                url,
                json.dumps(occurrences),
                str(error),
            )

            return False

        return True
