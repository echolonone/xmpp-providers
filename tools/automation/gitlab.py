#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Daniel Brötzmann <daniel.broetzmann@posteo.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Contains a bot which runs data gathering tools and manages GitLab
merge requests and commits.
"""

import logging

import gitlab

from tools.automation.web import WebBot
from tools.common import PROVIDERS_FILE_PATH

log = logging.getLogger()

GITLAB_INSTANCE_URL = "https://invent.kde.org"

MAIN_PROJECT_ID = 6857
MAIN_BRANCH = "master"

BOT_PROJECT_ID = 14015
BOT_AUTHOR = "XMPP Providers Bot"
BOT_USERNAME = "xmppprovidersbot"
BOT_MR_BRANCH = "providers-update"
BOT_MR_TITLE = "providers: Update"
BOT_MR_LABELS = ["Providers"]


class GitLabBot:
    """Runs bots for data gathering and manages GitLab merge requests and commits."""

    def __init__(self, main_token: str, bot_token: str, bot_email: str) -> None:
        """
        Parameters
        ----------
        main_token : str
            GitLab main project API access token
        bot_token : str
            GitLab bot personal API access token
        bot_email : str
            GitLab bot user email address for commits
        """

        log.info("Starting GitLab bot")

        self._bot_email = bot_email

        bot_gitlab_session = gitlab.Gitlab(
            url=GITLAB_INSTANCE_URL, private_token=bot_token
        )
        self._bot_project = bot_gitlab_session.projects.get(BOT_PROJECT_ID, lazy=False)

        main_gitlab_session = gitlab.Gitlab(
            url=GITLAB_INSTANCE_URL, private_token=main_token
        )
        self._main_project = main_gitlab_session.projects.get(
            MAIN_PROJECT_ID, lazy=False
        )

        self._prepare_bot_branch()

        # Run bots
        web_bot = WebBot()

        updated_providers = web_bot.get_updated_providers()

        self._prepare_merge_request()
        self._commit_changes(updated_providers)

    def _prepare_bot_branch(self) -> None:
        """Checks whether the bot's branch exists and creates it if not."""

        try:
            self._bot_project.branches.get(BOT_MR_BRANCH)
        except gitlab.exceptions.GitlabGetError:
            log.info("Creating new branch '%s'", BOT_MR_BRANCH)
            self._bot_project.branches.create(
                {
                    "branch": BOT_MR_BRANCH,
                    "ref": MAIN_BRANCH,
                }
            )
        else:
            log.debug("Merge request branch '%s' exists already", BOT_MR_BRANCH)

    def _prepare_merge_request(self) -> None:
        """Checks whether the bot's merge request exists and creates it if not."""

        merge_requests = self._main_project.mergerequests.list(
            state="opened", iterator=True
        )
        rebaseable_merge_request = None

        for merge_request in merge_requests:
            if (
                merge_request.title == BOT_MR_TITLE
                and merge_request.author["username"] == BOT_USERNAME
            ):
                rebaseable_merge_request = merge_request
                break

        if rebaseable_merge_request is None:
            log.info("Creating new merge request")
            merge_request = self._bot_project.mergerequests.create(
                {
                    "source_branch": BOT_MR_BRANCH,
                    "target_branch": MAIN_BRANCH,
                    "target_project_id": MAIN_PROJECT_ID,
                    "allow_collaboration": True,
                    "title": BOT_MR_TITLE,
                    "labels": BOT_MR_LABELS,
                }
            )
            log.debug("Merge request with ID '%s' created", merge_request.id)
        else:
            # The main project's repository access token is not allowed to push to
            # the bot repository.
            # Thus, if there is an open merge request, it cannot be rebased.
            # TODO: Rebase on MAIN_BRANCH.
            log.debug("Merge request is already open")
            # try:
            #     rebaseable_merge_request.rebase()
            # except gitlab.exceptions.GitlabMRRebaseError as err:
            #     log.info("Rebasing failed: %s", err)

    def _commit_changes(self, updated_providers: set[str]) -> None:
        """Creates a commit including all changes made by bots.

        Parameters
        ----------
        updated_providers : set[str]
            providers that have been updated
        """

        with open(PROVIDERS_FILE_PATH, "r") as providers_file:
            file_data = providers_file.read()

        data = {
            "author_name": BOT_AUTHOR,
            "author_email": self._bot_email,
            "branch": BOT_MR_BRANCH,
            "commit_message": self._generate_commit_message(updated_providers),
            "actions": [
                {
                    "action": "update",
                    "file_path": PROVIDERS_FILE_PATH,
                    "content": file_data,
                }
            ],
        }

        commit = self._bot_project.commits.create(data)
        log.info("Commit with ID '%s' created", commit.get_id())

    @staticmethod
    def _generate_commit_message(updated_providers: set[str]) -> str:
        """Generates a commit message for the updated providers.

        Parameters
        ----------
        updated_providers : set[str]
            providers that have been updated

        Returns
        -------
            commit message including the updated providers
        """

        message = f"{BOT_MR_TITLE} "

        for i, provider in enumerate(sorted(updated_providers)):
            if i != 0:
                message += ", "
            message += provider

        return message
