#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Melvin Keskin <melvo@olomono.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""
Contains criteria providers must match in order to be in a specific category.
"""

from datetime import datetime
from datetime import timedelta

CRITERIA_A = {
    "inBandRegistration": lambda v: v,
    "ratingXmppComplianceTester": lambda v: v == 100,
    "ratingImObservatoryClientToServer": lambda v: "A" in v,
    "ratingImObservatoryServerToServer": lambda v: "A" in v,
    "maximumHttpFileUploadFileSize": lambda v: v == 0 or v >= 20,
    "maximumHttpFileUploadTotalSize": lambda v: v == 0 or v >= 100,
    "maximumHttpFileUploadStorageTime": lambda v: v == 0 or v >= 7,
    "maximumMessageArchiveManagementStorageTime": lambda v: v == 0 or v >= 7,
    "professionalHosting": lambda v: v,
    "freeOfCharge": lambda v: v,
    "legalNotice": lambda v: len(v) != 0,
    "serverLocations": lambda v: len(v) != 0,
    "groupChatSupport,chatSupport,emailSupport": lambda v1, v2, v3: len(v1) != 0
    or len(v2) != 0
    or len(v3) != 0,
    "since": lambda v: datetime.fromisoformat(v) < datetime.now() - timedelta(365),
}

CRITERIA_B = {
    "inBandRegistration,registrationWebPage": lambda v1, v2: v1 or len(v2) != 0,
    "ratingXmppComplianceTester": lambda v: v == 100,
    "ratingImObservatoryClientToServer": lambda v: "A" in v,
    "ratingImObservatoryServerToServer": lambda v: "A" in v,
    "maximumHttpFileUploadFileSize": lambda v: v == 0 or v >= 20,
    "maximumHttpFileUploadTotalSize": lambda v: v == 0 or v >= 100,
    "maximumHttpFileUploadStorageTime": lambda v: v == 0 or v >= 7,
    "maximumMessageArchiveManagementStorageTime": lambda v: v == 0 or v >= 7,
    "professionalHosting": lambda v: v,
    "legalNotice": lambda v: len(v) != 0,
    "serverLocations": lambda v: len(v) != 0,
    "groupChatSupport,chatSupport,emailSupport": lambda v1, v2, v3: len(v1) != 0
    or len(v2) != 0
    or len(v3) != 0,
    "since": lambda v: datetime.fromisoformat(v) < datetime.now() - timedelta(365),
}

CRITERIA_C = {
    "inBandRegistration,registrationWebPage": lambda v1, v2: v1 or len(v2) != 0,
    "ratingXmppComplianceTester": lambda v: v >= 90,
    "ratingImObservatoryClientToServer": lambda v: "A" in v,
    "ratingImObservatoryServerToServer": lambda v: "A" in v,
    "maximumHttpFileUploadFileSize": lambda v: v >= 0,
    "maximumHttpFileUploadTotalSize": lambda v: v >= 0,
    "maximumHttpFileUploadStorageTime": lambda v: v >= 0,
    "maximumMessageArchiveManagementStorageTime": lambda v: v >= 0,
    "groupChatSupport,chatSupport,emailSupport": lambda v1, v2, v3: len(v1) != 0
    or len(v2) != 0
    or len(v3) != 0,
}

CRITERIA_D = {}
