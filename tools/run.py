#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Daniel Brötzmann <daniel.broetzmann@posteo.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""
Main entry point for XMPP Providers tools
"""

from __future__ import annotations

import logging
import sys
from argparse import ArgumentParser

from tools.automation.gitlab import GitLabBot
from tools.automation.web import WebBot
from tools.badge import BadgeGenerator
from tools.check_urls import CheckUrls
from tools.common import BADGE_COMMAND
from tools.common import Category
from tools.common import CHECK_URLS_COMMAND
from tools.common import FILTER_COMMAND
from tools.common import GITLAB_BOT_COMMAND
from tools.common import LOG_FORMAT
from tools.common import PRETTIFY_COMMAND
from tools.common import PROPERTY_MANAGER_COMMAND
from tools.common import WEB_BOT_COMMAND
from tools.filter import Filter
from tools.prettify import Prettify
from tools.property_manager import PropertyManager


class ToolsArgumentParser(ArgumentParser):
    """Parses arguments for XMPP Providers tools."""

    def __init__(self) -> None:
        super().__init__()

        self.description = """
        Provides tools for provider processing, maintenance and automation.
        """
        debug_group = self.add_mutually_exclusive_group()
        debug_group.add_argument(
            "-q",
            "--quiet",
            help="log only errors",
            action="store_const",
            dest="log_level",
            const=logging.ERROR,
            default=logging.INFO,
        )
        debug_group.add_argument(
            "-d",
            "--debug",
            help="log debug output",
            action="store_const",
            dest="log_level",
            const=logging.DEBUG,
            default=logging.INFO,
        )

        subparsers = self.add_subparsers(dest="command", parser_class=ArgumentParser)

        # badge command parsing
        badge_parser = subparsers.add_parser(
            "badge",
            help="""Generates badges for specific categories based on
            the provider lists which must be created beforehand.""",
        )
        badge_parser.add_argument(
            "-l",
            "--links",
            help="create symbolic links to badges instead of creating regular files",
            action="store_true",
            dest="create_symlink",
        )
        badge_parser.add_argument(
            "-c",
            "--count",
            help="""generate badges with count of providers in specific categories
                in addition to the normal badges""",
            action="store_true",
            dest="add_count",
        )
        badge_category_group = badge_parser.add_mutually_exclusive_group()
        badge_category_group.add_argument(
            "-A",
            "--category-A",
            help="generate only badge for category A",
            action="store_const",
            dest="category",
            const=Category.AUTOMATICALLY_CHOSEN,
            default=Category.ALL,
        )
        badge_category_group.add_argument(
            "-B",
            "--category-B",
            help="generate only badge for category B",
            action="store_const",
            dest="category",
            const=Category.MANUALLY_SELECTABLE,
            default=Category.ALL,
        )
        badge_category_group.add_argument(
            "-C",
            "--category-C",
            help="generate only badge for category C",
            action="store_const",
            dest="category",
            const=Category.COMPLETELY_CUSTOMIZABLE,
            default=Category.ALL,
        )
        badge_category_group.add_argument(
            "-D",
            "--category-D",
            help="generate only badge for category D",
            action="store_const",
            dest="category",
            const=Category.AUTOCOMPLETE,
            default=Category.ALL,
        )

        # check_urls command parsing
        check_urls_parser = subparsers.add_parser(
            "check_urls",
            help="""Checks whether URLs for specific files or all files
            (if no files are specified) are reachable.""",
        )
        check_urls_parser.add_argument(
            "files",
            nargs="*",
            metavar="FILES",
            help="files to validate URLs for",
        )

        # filter command parsing
        filter_parser = subparsers.add_parser(
            "filter",
            help="""Filters the providers and extracts those matching specific
            criteria.""",
        )
        filter_simple_group = filter_parser.add_mutually_exclusive_group()
        filter_simple_group.add_argument(
            "-s",
            "--simple",
            help="output only provider domains instead of their properties",
            action="store_true",
            dest="simple",
        )
        filter_simple_group.add_argument(
            "-c",
            "--categories",
            help="add the category of each provider to its entry",
            action="store_true",
            dest="categories",
        )
        filter_parser.add_argument(
            "-r",
            "--result-files",
            help="create additional files each containing the result of one provider",
            action="store_true",
            dest="result_files",
        )
        filter_category_group = filter_parser.add_mutually_exclusive_group()
        filter_category_group.add_argument(
            "-A",
            "--category-A",
            help="output only providers of category A",
            action="store_const",
            dest="category",
            const=Category.AUTOMATICALLY_CHOSEN,
            default=Category.ALL,
        )
        filter_category_group.add_argument(
            "-B",
            "--category-B",
            help="output only providers of category B",
            action="store_const",
            dest="category",
            const=Category.MANUALLY_SELECTABLE,
            default=Category.ALL,
        )
        filter_category_group.add_argument(
            "-C",
            "--category-C",
            help="output only providers of category C",
            action="store_const",
            dest="category",
            const=Category.COMPLETELY_CUSTOMIZABLE,
            default=Category.ALL,
        )
        filter_category_group.add_argument(
            "-D",
            "--category-D",
            help="output only providers of category D",
            action="store_const",
            dest="category",
            const=Category.AUTOCOMPLETE,
            default=Category.ALL,
        )
        filter_parser.add_argument(
            "providers",
            help="domains of providers being filtered",
            nargs="*",
        )

        # prettify command parsing
        subparsers.add_parser(
            "prettify",
            help="Validates JSON files and applies a consistent format to them.",
        )

        # property manager parsing
        subparsers.add_parser(
            PROPERTY_MANAGER_COMMAND,
            help="Manages the properties that providers have.",
        )

        # automation commands
        # gitlab_bot command parsing
        gitlab_bot = subparsers.add_parser(
            "gitlab_bot",
            help="""Runs bots for data gathering and manages GitLab merge
            requests and commits.""",
        )
        gitlab_bot.add_argument(
            "gitlab_main_token",
            help="GitLab main repository API access token",
        )
        gitlab_bot.add_argument(
            "gitlab_bot_token",
            help="GitLab bot user API access token",
        )
        gitlab_bot.add_argument(
            "gitlab_bot_email",
            help="GitLab bot user email address for commits",
        )

        # web_bot command parsing
        web_bot_parser = subparsers.add_parser(
            "web_bot", help="Queries XMPP provider ratings or triggers their updates."
        )
        web_bot_parser.add_argument(
            "-u",
            "--update",
            action="store_true",
            help="trigger update of provider ratings (no query is run)",
        )


if __name__ == "__main__":
    arguments = ToolsArgumentParser().parse_args(
        args=None if sys.argv[1:] else ["--help"]
    )

    logging.basicConfig(level=arguments.log_level, format=LOG_FORMAT)

    if arguments.command == CHECK_URLS_COMMAND:
        CheckUrls(arguments.files)
    elif arguments.command == BADGE_COMMAND:
        BadgeGenerator(
            arguments.category,
            arguments.add_count,
            arguments.create_symlink,
        )
    elif arguments.command == FILTER_COMMAND:
        Filter(
            arguments.category,
            arguments.providers,
            arguments.simple,
            arguments.categories,
            arguments.result_files,
        )
    elif arguments.command == PRETTIFY_COMMAND:
        Prettify()
    elif arguments.command == PROPERTY_MANAGER_COMMAND:
        PropertyManager()
    elif arguments.command == GITLAB_BOT_COMMAND:
        GitLabBot(
            arguments.gitlab_main_token,
            arguments.gitlab_bot_token,
            arguments.gitlab_bot_email,
        )
    elif arguments.command == WEB_BOT_COMMAND:
        WebBot(update=arguments.update)
